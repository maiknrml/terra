extends KinematicBody2D

var SPEED = 70
onready var obj = get_parent().get_node("Player")

var velocity = Vector2()

var is_dead = false
var health = 3

var dead_time = 0
var deadt = false

func dead():
	health = health - 1
	if health <= 0:
		is_dead = true
		velocity = Vector2(0, 0)
		$AnimatedSprite.play("dead")
		globis.kills = globis.kills + 1
		$CollisionShape2D.call_deferred("set_disabled", true)
		deadt = true
		var random = randi()%20
		if random == 0:
			globis.ice = globis.ice +1 
		elif random == 1:
			globis.gras = globis.gras +1 
		elif random == 2:
			globis.fire = globis.fire +1 

func _physics_process(delta):
	if deadt == true:
		dead_time = dead_time +1
		if dead_time >= 100:
			$AnimatedSprite.play("explosion")
	elif is_dead == false:
		var dir = (obj.global_position - global_position).normalized()
		move_and_collide(dir * SPEED * delta)
		$AnimatedSprite.play("walk")







