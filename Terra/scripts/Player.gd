extends KinematicBody2D

const MOVE_SPEED = 300

const FIREBALL = preload("res://Scenes/fireball.tscn")
const MAGICBULLET = preload("res://Scenes/MagicBullet.tscn")
const ENEMY = preload("res://Scenes/Enemy.tscn")
const ENEMY2 = preload("res://Scenes/Enemy2.tscn")
const ENEMY3 = preload("res://Scenes/Enemy3.tscn")

enum Weapon 	{Fist, Sword, Bow, Handgun, Rifle, MagicRod}
enum Direction 	{idle, up, right, down, left, upRight, upLeft, downRight, downLeft} 

var walkDirection 	= Direction.idle
var Equipped_Weapon = Weapon.MagicRod

var looksLeft = false
var isIdle = true;
var i = 0
var count = 0
var fik = Vector2(400.0,400.0)
var fik2 = Vector2(400.0,400.0)
var fik3 = Vector2(400.0,400.0)

var boost = false
var count2 = 0
var cd = 0
var cd_start = false
var cd_set = false

#HEALTH:
var health_player = 2
var is_dead = false

var m_magicCounter = 6

const m_SHOOTTIMERCONST = 10
var m_ShootTimer = m_SHOOTTIMERCONST
var m_shootCounter = m_ShootTimer


var look_vec = get_global_mouse_position() - global_position


func _ready():
	yield(get_tree(), "idle_frame")
	get_tree().call_group("zombies", "set_player", self)


func _translateDirection(p_DirVec = Vector2()):
	isIdle = false
	
	if p_DirVec.x == 0 && p_DirVec.y == 0:
		_moveIdle()
		
	if p_DirVec.x == 0 && p_DirVec.y == 1:
		_moveUp()
		
	if p_DirVec.x == 1 && p_DirVec.y == 1:
		_moveUpRight()
		
	if p_DirVec.x == 1 && p_DirVec.y == 0:
		_moveRight()
		
	if p_DirVec.x == 1 && p_DirVec.y == -1:
		_moveDownRight()
		
	if p_DirVec.x == 0 && p_DirVec.y == -1:
		_moveDown()
		
	if p_DirVec.x == -1 && p_DirVec.y == -1:
		_moveDownLeft()
		
	if p_DirVec.x == -1 && p_DirVec.y == 0:
		_moveLeft()
	
	if p_DirVec.x == 1 && p_DirVec.y == -1:
		_moveUpLeft()


func _moveIdle():
	isIdle = true
	walkDirection = Direction.idle		#ToDo fertig machen
	if   (looksLeft == false):
		$AnimatedSprite.play("idle_right")
	else:
		$AnimatedSprite.play("idle_left")
		#$AnimatedSprite.play("walk_up")

func _moveLeft():
	looksLeft = true
	walkDirection = Direction.left
	$AnimatedSprite.play("walk_left")

func _moveRight():
	looksLeft = false
	walkDirection = Direction.right
	$AnimatedSprite.play("walk_right")

func _moveUp():
	walkDirection = Direction.up
	_diagonalMove();

func _moveDown():
	walkDirection = Direction.down
	_diagonalMove();

func _moveUpRight():
	walkDirection = Direction.upRight
	_diagonalMove();

func _moveDownRight():
	walkDirection = Direction.downRight
	_diagonalMove();

func _moveUpLeft():
	walkDirection = Direction.upLeft
	_diagonalMove();

func _moveDownLeft():
	walkDirection = Direction.downLeft
	_diagonalMove();

func _diagonalMove():
	if   (looksLeft == false):
		$AnimatedSprite.play("walk_right")
	else:
		$AnimatedSprite.play("walk_left")
		#$AnimatedSprite.play("walk_up")
	
func _handleMove(p_Delta):


	if is_dead == true:
		$AnimatedSprite.play("dead")
	else:
		var move_vec = Vector2()
		isIdle = true;
		if Input.is_action_pressed("move_left"):
			move_vec.x -= 1

		elif Input.is_action_pressed("move_right"):
			move_vec.x += 1

		if Input.is_action_pressed("move_up"):
			move_vec.y -= 1

		elif Input.is_action_pressed("move_down"):
			move_vec.y += 1
		if cd_set == false:
			if Input.is_action_just_pressed("ui_boost"):
				boost = true
				cd_set = true
		if boost == true:
			_translateDirection(move_vec)
			move_vec = move_vec.normalized()
			move_and_collide(move_vec * MOVE_SPEED * p_Delta * 3)
		else:
			_translateDirection(move_vec)
			move_vec = move_vec.normalized()
			move_and_collide(move_vec * MOVE_SPEED * p_Delta)
			
		


func _attackShoot(p_look_vec = Vector2()):
	var fireball = FIREBALL.instance()
	var normalV = Vector2()
	var winkel  = 0
	normalV.x = 0
	normalV.y = 1
	
	get_parent().add_child(fireball)

	fireball.position = ($Position2D.global_position + (fireball.m_Abstand * p_look_vec))
	fireball.velocity = p_look_vec.normalized() * fireball.SPEED
	
	if(p_look_vec.x > 0):
		winkel = (p_look_vec.normalized().dot(normalV) * 90)
	else:
		winkel = 180 - (p_look_vec.normalized().dot(normalV) * 90)

	fireball.rotate(deg2rad(winkel))
	
	
func _attackMagic(p_fireMode, p_winkel, p_look_vec = Vector2()):
	var fireball = MAGICBULLET.instance()
	var spriteFrame = p_fireMode
	
	get_parent().add_child(fireball)

	fireball.position = ($Position2D.global_position + (fireball.m_Abstand * p_look_vec))
	fireball.velocity = p_look_vec.normalized() * fireball.SPEED
	

	fireball.rotate(deg2rad(p_winkel))
	
	m_ShootTimer = m_SHOOTTIMERCONST
	
	match p_fireMode:
		6:	#Feuerball		Double Damage
			fireball.DAMAGE = fireball.DAMAGE * 4
			fireball._scaleBullet(2)
			m_ShootTimer = m_SHOOTTIMERCONST * 2
		7:	#Eisball		Fast Shot
			#fireball._scaleBullet(2)
			fireball.SPEED = fireball.SPEED / 6
		8:	#YellowMagic	Bullet Stream
			m_ShootTimer = m_SHOOTTIMERCONST / 4
			fireball.DAMAGE = 1
		9:	#RedMagic		Piercing Shot
			fireball.m_piercing = 4
		10:	#BlueMagic
			m_ShootTimer = m_SHOOTTIMERCONST / 2
		11: #ScatterShot
			spriteFrame = 10
#				var scaleO = Vector2()
#				scaleO.x = 5
#				scaleO.y = 5
			_attackMagic(spriteFrame, ((rad2deg(p_winkel)-30)), p_look_vec)
			_attackMagic(spriteFrame, ((rad2deg(p_winkel)+30)), p_look_vec)
	
	fireball.sprite.frame = spriteFrame
	
func _handleAttack(p_Delta):
	look_vec = get_global_mouse_position() - global_position
	look_vec = look_vec.normalized()
	
	var normalV = Vector2()
	var winkel  = 0
	normalV.x = 0
	normalV.y = 1
	
	if(look_vec.x > 0):
		winkel = (look_vec.normalized().dot(normalV) * 90)
	else:
		winkel = 180 - (look_vec.normalized().dot(normalV) * 90)
	
	if Input.is_action_just_pressed("Weapon 1"):
		m_magicCounter = 6
	if Input.is_action_just_pressed("Weapon 2"):
		m_magicCounter = 7
	if Input.is_action_just_pressed("Weapon 3"):
		m_magicCounter = 8
	if Input.is_action_just_pressed("Weapon 4"):
		m_magicCounter = 9
	if Input.is_action_just_pressed("Weapon 5"):
		m_magicCounter = 10
	if Input.is_action_just_pressed("Huenchen"):
		m_magicCounter = 4
		_huenchenPower(look_vec)

	if Input.is_action_just_pressed("Cycle Weapon"):
		if m_magicCounter == 11:
			m_magicCounter = 6
		else:
			m_magicCounter = m_magicCounter+1

	if m_shootCounter < m_ShootTimer:
			m_shootCounter += 1
	else:
		if Input.is_action_pressed("shoot"):
			m_shootCounter = 0
			match Equipped_Weapon:
				Weapon.Fist:
					i
				Weapon.Sword:
					i
				Weapon.Bow:
					_attackShoot(look_vec)
				Weapon.Handgun:
					i
				Weapon.Rifle:
					i
				Weapon.MagicRod:
					_attackMagic(m_magicCounter, winkel, look_vec)
	if Input.is_action_just_pressed("Magic Mine"):
		_huenchenPower(look_vec)

func _huenchenPower(p_look_vec = Vector2()):
	var Power = 9001
	if Power > 9000:
		var fireball = MAGICBULLET.instance()
		var normalV = Vector2()
		var winkel  = 0
		normalV.x = 0
		normalV.y = 1
		
		get_parent().add_child(fireball)
	
		fireball.position = ($Position2D.global_position + (fireball.m_Abstand * p_look_vec))
		fireball.velocity = p_look_vec.normalized() * fireball.SPEED
		
		if(p_look_vec.x > 0):
			winkel = (p_look_vec.normalized().dot(normalV) * 90)
		else:
			winkel = 180 - (p_look_vec.normalized().dot(normalV) * 90)

		fireball.rotate(deg2rad(winkel))
		fireball.velocity = fireball.velocity / 9001
		fireball.sprite.frame = 4
		fireball.DAMAGE = 80

func _physics_process(delta):
	var direction = Vector2()
	var velocity = Vector2()
	
	_handleMove(delta)
	_handleAttack(delta)

	if Input.is_action_just_pressed("Pause"):
		get_tree().change_scene("res://Scenes/RotateEarths.tscn")
	
	if boost == true:
		count2 = count2 + 1
		if count2 >= 25:
			boost = false
			count2 = 0
			cd_start = true
	
	if cd_start == true:
		cd = cd + 1
		if cd >= 100:
			cd_start = false
			cd = 0
			cd_set = false
	
	count += 1
	if count == 100:
		
	#if Input.is_action_just_pressed("spawn"):
		var enemy = ENEMY.instance()

		get_parent().add_child(enemy)
		randomize()
		var rand = randi()%4
		if rand == 0:
			fik = Vector2(randi()%500,500.0)
		if rand == 1:
			fik = Vector2(-randi()%500,500.0)
		if rand == 2:
			fik = Vector2(500.0,-randi()%500)
		if rand == 3:
			fik = Vector2(-500.0,-randi()%500)

		enemy.position = $Position2D.global_position + fik
		
		#Fledermaus:
		var enemy2 = ENEMY2.instance()

		get_parent().add_child(enemy2)
		randomize()
		var rand2 = randi()%4
		if rand2 == 0:
			fik2 = Vector2(400.0,400.0)
		if rand2 == 1:
			fik2 = Vector2(-400.0,400.0)
		if rand2 == 2:
			fik2 = Vector2(400.0,-400.0)
		if rand2 == 3:
			fik2 = Vector2(-400.0,-400.0)

		enemy2.position = $Position2D.global_position + fik2
		
		#Lego-Monster:
		var enemy3 = ENEMY3.instance()

		get_parent().add_child(enemy3)
		randomize()
		var rand3 = randi()%4
		if rand3 == 0:
			fik3 = Vector2(400.0,400.0)
		if rand3 == 1:
			fik3 = Vector2(-400.0,400.0)
		if rand3 == 2:
			fik3 = Vector2(400.0,-400.0)
		if rand3 == 3:
			fik3 = Vector2(-400.0,-400.0)

		enemy3.position = $Position2D.global_position + fik3

		count = 0


func kill():
	get_tree().reload_current_scene()



func _on_Hitbox_body_entered(body):
	health_player = health_player - 1
	print(health_player)
	if is_dead == false:
		$AnimatedSprite.play("idle_right")
	if(health_player <= 0):
		is_dead = true
		var velocity = Vector2(0, 0)
		$AnimatedSprite.play("dead")
		



