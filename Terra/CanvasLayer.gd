extends CanvasLayer
var won = true
var item1= "buch"
var item2= "ball"
var amountMax = 5
var amountCollectedBuch = 2
var amountCollectedBall = 3

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("Collected").text="You collected the " + '\n' + "following items: " + '\n' + str(item1) + ": " + str(amountCollectedBuch)+"/"+str(amountMax) + '\n' + str(item2) + ": " + str(amountCollectedBall)+"/"+str(amountMax)
	pass 

func _process(delta):
	if won == true:
		get_node("WinLose").text="You Win!"
		get_node("Button").text="Next evolution?"
	else:
		get_node("WinLose").text="You Lose!"
		get_node("Button").text="Retry? "
	
