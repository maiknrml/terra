extends Button

var kills = 100
var amountOfKills=0
var flowers = 10
var amountOfFlowers=0
var portals = 3
var amountOfPortals=0
var chests = 2
var amountOfChests = 0
var clickCounter = 0


func _ready():
	pass

func _process(delta):
	get_parent().get_node("Quest1").text="Kill " + str(kills) + " Units: "+str(amountOfKills)+ "/"+ str(kills)+" done"
	get_parent().get_node("Quest2").text="Collect " +str(flowers)+" flowers: " +str(amountOfFlowers)+"/"+str(flowers)+" done"
	get_parent().get_node("Quest3").text="Jump through " +str(portals)+ " portals: "+str(amountOfPortals)+"/"+str(portals)+" done"
	get_parent().get_node("Quest4").text="Open " +str(chests)+ " chests: "+str(amountOfChests)+"/"+str(chests)+" done"
	if amountOfKills==100:
		$"Ladebalken".region_rect.position.x = 320
	if amountOfFlowers==10:
		$"Ladebalken".region_rect.position.x = 480
	if amountOfPortals==3:
		$"Ladebalken".region_rect.position.x = 640
	if amountOfChests==2:
		$"Ladebalken".region_rect.position.x = 800
	

func _on_Button_pressed():
	if clickCounter == 0:
		amountOfKills=100
	elif clickCounter == 1:
		amountOfFlowers=10
	elif clickCounter == 2:
		amountOfPortals=3
	elif clickCounter:
		amountOfChests=2
	clickCounter += 1
	#$Button.disabled = true
	#next evolution step
