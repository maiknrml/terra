extends CanvasLayer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	
func _on_TextEdit_text_changed():
        var lab = get_node("Label")
        var edit = get_node("TextEdit")
        lab.set_text(edit.get_text())

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
