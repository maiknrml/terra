extends Node

var current_map_size = Vector2(100,100)
var percentage_floors = 45.0

var neighbor_dir = [Vector2(1,0), Vector2(1,1), Vector2(0,1),
					Vector2(-1,0), Vector2(-1,-1), Vector2(0,-1),
					Vector2(1,-1), Vector2(-1,1)]

var neighbor = [Vector2(1,0),  Vector2(0,1),
					Vector2(-1,0),  Vector2(0,-1)]


func _ready():
	randomize()
	make_map()
	smooth_map()
	smooth_map()
	smooth_map()
	#deleter()
	testler()
	globis.kills = 0
	globis.boost_bar = 100


#func _process(delta):
	

func make_map():
	for x in range(1,current_map_size.x-1):
		for y in range(1, current_map_size.y-1):
			var num = rand_range(0.0,100.0)
			if num < percentage_floors:
				$Map.set_cell(x,y, randi()%20)
			else:
				$Map.set_cell(x,y, randi()%20+19)
	for x in [0, current_map_size.x - 1]:
		for y in current_map_size.y:
			$Map.set_cell(x,y, randi()%20+19)
	
	
	for x in current_map_size.x:
		for y in [0, current_map_size.y - 1]:
			$Map.set_cell(x, y, randi()%20+19)
			

func smooth_map():
	for x in range (1, current_map_size.x - 1):
		for y in range (1, current_map_size.y - 1):
			var number_of_neighbor_walls = 0
			for direction in neighbor_dir:
				var current_tile = Vector2(x,y) + direction
				if $Map.get_cell(current_tile.x, current_tile.y) > 19:
					number_of_neighbor_walls += 1 
			if number_of_neighbor_walls > 4:
				$Map.set_cell(x,y, randi()%20+19)
			elif number_of_neighbor_walls < 4:
				$Map.set_cell(x,y, randi()%20)


func testler():
	for x in range (0, current_map_size.x):
		for y in range (0, current_map_size.y):
			var chance = randi()%200
			if chance < 1:
				$Overlay.set_cell(x/0.267,y/0.267,randi()%7)

func _on_Area2D_body_entered(body):
	if body.name == "Player":
		get_tree().change_scene("res://Scenes/Options.tscn")
