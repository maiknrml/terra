extends Area2D

var SPEED = 20
var DAMAGE = 50

var m_piercing = 0

onready var sprite = $Bullet_Sheet

var velocity = Vector2()
var m_Abstand = Vector2(60, 60)
var i

func _ready():
	pass

func _physics_process(delta):

	translate(velocity)
	#$AnimatedSprite.play("shoot")
	
func _spawnShot(p_parentPosition, p_look_vec = Vector2()):
	self.position = (p_parentPosition)# + (m_Abstand * p_look_vec))
	self.velocity = p_look_vec.normalized() #* SPEED
	
	
func _on_fireball_body_entered(body):
	if "Enemy" in body.name:
		body.dead()
		if (m_piercing > 0):
			m_piercing = m_piercing -1
		else:
			queue_free()

func _wildMagic(fun):#
	var scaleO = Vector2()
	scaleO.x = 5
	scaleO.y = 5
	sprite.frame = fun
	match fun:
		6:
			 transform.scaled(scaleO)
		7:
			i
		8:
			i
		9:
			i
		10:
			i

func _scaleBullet(p_scale):
	$BulletShape2D.scale.x = p_scale
	$BulletShape2D.scale.y = p_scale
	$Bullet_Sheet.scale.x  = p_scale
	$Bullet_Sheet.scale.y  = p_scale
