extends Area2D

const SPEED = 20
const DAMAGE = 50


var velocity = Vector2()
var m_Abstand = Vector2(100, 100)

func _ready():
	pass

func _physics_process(delta):

	translate(velocity)
	$AnimatedSprite.play("shoot")
	
func _spawnShot(p_parentPosition, p_look_vec = Vector2()):
	self.position = (p_parentPosition)# + (m_Abstand * p_look_vec))
	self.velocity = p_look_vec.normalized() #* SPEED
	
	
func _on_fireball_body_entered(body):
	if "Enemy" in body.name:
		body.dead()
	queue_free()



